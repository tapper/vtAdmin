<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PagesController@getHome');

Route::get('/about','PagesController@getAbout');

Route::get('/contact','PagesController@getContact');

Route::post('/contact/submit','MessagesController@getAbout');

Route::get('/messages','MessagesController@getMessages');


//Route::resource('users','UserController');
//Route::resource('company_old','CompaniesController');
//Route::resource('company_categories.companies','CompaniesController');
//Route::resource('CompanyCategories','CompanyCategoriesController');

//Route::resource('companies.managers','ManagerController');
//Route::resource('companies.coupons','CompanyCouponsController');
//Route::resource('company_old/{{id}}/coupons/{{couponId}}','CompanyCouponsController');
//Route::get('/forms','FormsController@returnForm');

Route::resource('Coupons','CouponController');
Route::resource('Coachs','CoachController');
Route::resource('Courses','CourseController');
Route::resource('Companies','CompaniesController');
Route::resource('Customers','CustomersController');








