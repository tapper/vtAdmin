<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Coupon extends Model
{
    use Mediable;
    protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['title','price','description','quantity'];
    protected $with = ['mediables'];
    protected $appends = ['logo','name'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function mediables()
    {
        return $this->hasOne(Mediables::class , 'mediable_id');
    }

    public function getLogoAttribute()
    {
        $company = Company::find($this->company_id);
        return $company->logo;
    }

    public function getNameAttribute()
    {
        $company = Company::find($this->company_id);
        return $company->title;
    }
}
