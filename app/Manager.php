<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    //protected $with=['company_old'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
