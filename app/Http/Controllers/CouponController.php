<?php

namespace App\Http\Controllers;

use App\Company;
use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
Use Redirect;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Coupons = Coupon::all();
        $CompanyName = '';
        //echo json_encode($Coupons);
        $isEdit = true;
        $Delete = true;
        $Description = 'רשימת כל הקופונים של חברת';
        $Url = "coupons";
        $fileds = array('מספר קופון','נושא הקופון','מחיר','כמות קיימת');
        $rows = array('id','title','price','quantity');
        return view('coupon.index', ['CompanyName' => $CompanyName,'Logo' => '','Title' => 'עסקים' ,'Description' => strip_tags($Description)  ,'Categories' => $Coupons , 'fileds' => $fileds , 'rows' => $rows , 'isEdit' => $isEdit , 'Delete' => $Delete ,'url' =>$Url]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // return View::make('coupon.create');
        return view('coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coupon = new Coupon();
        $coupon->company()->associate(Company::find(1));
        $coupon->fill($request->all());
        $coupon->save();

        return redirect('/coupons');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::Find($id);
        $Coupons = $company->coupons;
        //echo json_encode($company);
        $CompanyLogo = $company->logo;
        $CompanyName = $company->title;

        $isEdit = true;
        $Delete = true;
        $Description = 'רשימת כל הקופונים של חברת';
        $Url = "coupons";
        $fileds = array('','נושא הקופון','מחיר','כמות קיימת');
        $rows = array('id','title','price','quantity');
        return view('coupon.index', ['CompanyName' => $CompanyName,'Logo' => $CompanyLogo,'Title' => 'עסקים' ,'Description' => $Description  ,'Categories' => $Coupons , 'fileds' => $fileds , 'rows' => $rows , 'isEdit' => $isEdit , 'Delete' => $Delete ,'url' =>$Url]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        echo json_encode($coupon);
        return view('coupon.edit', ['coupon' => $coupon]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        $coupon->fill($request->all());
        $coupon->save();
        return redirect('/coupons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        return redirect('/coupons');
    }
}
