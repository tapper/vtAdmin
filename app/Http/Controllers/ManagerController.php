<?php

namespace App\Http\Controllers;

use App\Company;
use App\Manager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {
        //$CompanyId = $request->get('companyId');
        //$Managers = Manager::where('company_id', $CompanyId)->get();
        //echo json_encode($company_old);
        $Managers = $company->managers;
        $isEdit = true;
        $Delete = true;
        $Url = "/manager";
        $fileds = array('','שם המנהל','אימייל');
        $rows = array('id','name','email');

        return view('manager.index', ['name' => 'id','Title' => 'עסקים' ,'Description' => 'רשימת כל בתי העסק הקיימים'  ,'Categories' => $Managers , 'fileds' => $fileds , 'rows' => $rows , 'isEdit' => $isEdit , 'Delete' => $Delete ,'url' =>$Url]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
