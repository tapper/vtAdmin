<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
Use Redirect;

class UserController extends Controller
{

    public function index()
    {
        echo "shay";
        $Categories = User::all();
        $isEdit = true;
        $Delete = true;
        $Url = "/users";
        $fileds = array('','שם הלקוח','כתובת מייל','טלפון','עיר','מספר נקודות');
        $rows = array('id','name','email','phone3','city','points');
        $title = "לקוחות רשומים";
        return view('users.index', ['name' => 'id','Title' => $title ,'Description' => '..'  ,'Categories' => $Categories , 'fileds' => $fileds , 'rows' => $rows , 'isEdit' => $isEdit , 'Delete' => $Delete ,'url' =>$Url]);
    }


    public function create()
    {
        $Url = 'UsersController';
        return view('users.create', ['url' => $Url]);
    }


    public function store(Request $request)
    {
        print_r($request->all());
        $item = new User();
        $item->fill($request->all());
        $item->save();

        return redirect('/users');
    }


    public function show($id)
    {

    }


    public function edit(User $user)
    {
        return view('users.edit', ['item' => $user]);
    }


    public function update(Request $request, User $user)
    {
        print_r($request->all());
        $user->fill($request->all());
        $user->save();
        return redirect('/users');
    }


    public function destroy(User $user)
    {
        //echo "OK";
        //print_r($user);
        $user->delete();
        return redirect('/users');
    }
}
