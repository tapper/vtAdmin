@extends('layouts.app')

@section('content')
    <div class="formWrapper">
        <div class="row DirectionRtl">
            <div class="col-lg-12">
                <h1 class="page-header">הוספת קופון חדש</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row DirectionRtl" >
            <div class="col-lg-12">
                <div class="panel panel-default" >
                    <div class="panel-heading " >
                        עדכן קופון חדש
                    </div>
                    <div class="panel-body ">
                        <div class="row ">
                            <div class="col-lg-12">
                                <form role="form" action="{!! action('UserController@update', ['id' => $coupon->id ] ) !!}"  method="post" >


                                    <input name="_method" type="hidden" value="PATCH" >
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>כותרת הקופון</label>
                                                <input class="form-control" value="{{$coupon->title}}" name="title">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>מחיר הקופון</label>
                                                <input class="form-control" value="{{$coupon->price}}" name="price">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>תיאור על הקופון</label>
                                                <input class="form-control" value="{{$coupon->description}}" name="description">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>כמות קופונים קיימת</label>
                                                <input class="form-control" value="{{$coupon->quantity}}" name="quantity">
                                            </div>
                                        </div>

                                    </div>



                                    <button type="submit" class="btn btn-default DirectionLtr" style="float:left; width: 120px; text-align: center !important;" >שלח קופון</button>
                                    {{csrf_field()}}
                                </form>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

@endsection





