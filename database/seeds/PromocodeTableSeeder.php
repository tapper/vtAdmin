<?php

use Illuminate\Database\Seeder;

class PromocodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $code = new \App\Models\Code();
        $code->rewardable_id = 1;
        $code->rewardable_type = 'App\Models\User';
        $code->code = 'QWERTY';
        $code->reward = 500;
        $code->label = 1;
        $code->save();
    }
}
